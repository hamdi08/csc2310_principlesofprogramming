package day1Examples;

public class MatrixAdd {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int dim1 = 2;
		int dim2 = 3;
		int a[][] = {{4,5,6}, {7,8,9}};
		int b[][] = {{1,2,3}, {0,1,2}};
		
		System.out.println("Matrix a...");
		for(int i=0;i<dim1;i++)
		{
			for(int j=0;j<dim2;j++)
				System.out.print(a[i][j]+"\t");
			System.out.println();
		}
		System.out.println("Matrix b...");
		for(int i=0;i<dim1;i++)
		{
			for(int j=0;j<dim2;j++)
				System.out.print(b[i][j]+"\t");
			System.out.println();
		}
		int c[][] = new int[dim1][dim2];
		System.out.println("Sum of a and b: Matrix c...");
		for(int i=0;i<dim1;i++)
		{
			for(int j=0;j<dim2;j++)
			{
				c[i][j] = a[i][j] + b[i][j];
				System.out.print(c[i][j]+"\t");
			}			
			System.out.println();
		}
		

	}

}
