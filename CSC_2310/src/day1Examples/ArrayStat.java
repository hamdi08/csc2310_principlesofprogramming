package day1Examples;
import java.util.*;
public class ArrayStat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i, n, array[];
		Scanner in = new Scanner(System.in);
		System.out.println("Enter number of elements");
	    n = in.nextInt(); 
	    array = new int[n];
	 
	    System.out.println("Enter " + n + " integers");
	 
	    for (i = 0; i < n; i++)
	      array[i] = in.nextInt();
	    in.close();
	    
	    Arrays.sort(array);
	    //Min
	    int min = array[0];
	    //Max
	    int max = array[n-1];
	    //Mean
	    int sum = 0;
	    double mean;
	    for(i=0; i<n;i++)
	    	sum += array[i];
	    mean = (double) sum/n;
	    //Median
	    double median;
	    if(n%2 == 1)
	    	median = (double)array[n/2];
	    else
	    	median = (double)((array[n/2] + array[n/2 - 1])/2);
	    
	    //Display
	    System.out.println("Max: " + max);
	    System.out.println("Min: " + min);
	    System.out.println("Mean: "+ mean);
	    System.out.println("Median: "+ median);

	}

}
