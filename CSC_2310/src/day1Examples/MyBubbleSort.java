package day1Examples;

public class MyBubbleSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {10,34,2,56,7,67,88,42};
		//bubble sort
		int temp;
		for(int i=0; i<arr.length-1;i++)
			for(int j=i+1;j<arr.length;j++)
				if(arr[j]<arr[i])
				{
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
		//Display
		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i]+", ");

	}

}
