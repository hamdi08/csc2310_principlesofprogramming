package day1Examples;
import java.io.*;

public class ReadTextFile {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		FileReader fr = new FileReader("testInput.txt");
		BufferedReader br = new BufferedReader(fr);
		String s;
		
		while ((s = br.readLine())!=null)
			System.out.println(s);
		fr.close();

	}

}
