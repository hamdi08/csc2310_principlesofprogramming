package Jyoti;
import java.io.*;

//Keyboard to Disk utility
public class KtoD 
{
	public static void main(String[] args) throws IOException
	{
		String str;
		FileWriter fw;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try
		{
			fw = new FileWriter("testOutput.txt",true);//false: overwrite existing file; true:append to existing file
		}
		catch(IOException ex)
		{
			System.out.println("Cannot open file.");
			return;
		}
		System.out.println("Enter text ('stop' to quit).");
		do
		{
			System.out.println(": ");
			str = br.readLine();
			if (str.compareTo("stop")==0)
				break;
			str = str + "\r\n";
			fw.write(str);
		} while(str.compareTo("stop")!=0);
		fw.close();
		System.out.println("TestOutput.txt is updated.");
	}
}
