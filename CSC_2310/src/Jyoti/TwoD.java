package Jyoti;

public class TwoD 
{
	public int data;
	public static void main(String[] args) 
	{
		int t, i;
		
		int table[][] = new int[3][4];
		System.out.println("use of two-dimensional array of int:");
		for (t=0; t<3; ++t)
		{
			for (i=0;i<4;++i)
			{
				table[t][i]=(t*4)+i+1;
				System.out.print(table[t][i] + " ");
			}
			System.out.println();
		}
		
		System.out.println("use of two-dimensional array of object:");
		TwoD twoD[][] = new TwoD[3][4];
		for (t=0; t<twoD.length; t++)
		{
			for (i=0; i<twoD[t].length; i++)
			{
				twoD[t][i] = new TwoD();
				twoD[t][i].data = (t*4) + i +1;
				System.out.print(twoD[t][i].data + " ");
			}
			System.out.println();
		}
	}
}