package Jyoti;
//package com.george;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ReadFile {

    public static void main(String[] args){
    	Path file = Paths.get("C:\\Users\\Rakesh\\Desktop\\file.txt");

    	if(Files.exists(file) && Files.isReadable(file)) {

    	    try {
    	        // File reader
    	        BufferedReader reader = Files.newBufferedReader(file, Charset.defaultCharset());
    	        List<Integer> numbers = new ArrayList<>();
     	       
    	        String line;
    	        // read each line
    	        while((line = reader.readLine()) != null) {
    	            System.out.println(line);
	    	            try{	
	    	            	 StringTokenizer tokenizer = new StringTokenizer(line, ",");
	    	                 while (tokenizer.hasMoreElements()) {
	    	                     // parse each integer in file
	    	                     int i = Integer.parseInt(tokenizer.nextToken());
	 	    	            	numbers.add(i);
	    	                 }
	    	            } catch (NumberFormatException e) {
	    	    	        e.printStackTrace();
	    	    	    }
    	                
    	            }
    	        System.out.println("Element[0]=="+numbers.get(0));
    	        List<Integer> matrixArray1= new ArrayList<>();
    	        List<Integer> matrixArray2= new ArrayList<>();
    	        
    	        int expectedLength=((numbers.get(0)*numbers.get(1))+(numbers.get(2)*numbers.get(3)));
    	        System.out.println("===================");
    	        System.out.println(numbers.get(0)+"-"+numbers.get(1)+"-"+(numbers.get(2)+"-"+numbers.get(3)));
    	        System.out.println("===================");
    	        System.out.println("Element[0]=="+numbers.get(0));
    	        System.out.println("numbers.size()=="+numbers.size());
    	        System.out.println("expectedLength=="+expectedLength);
    	        
    	        if(numbers.size()==4+expectedLength){
    	        	System.out.println("Element Matrix1 ==");
    	        	for(int m=4;m<4+numbers.get(0)*numbers.get(1);m++){
    	        		System.out.println(numbers.get(m));
    	        		matrixArray1.add(numbers.get(m));
    	        	}
    	        	System.out.println("Element Matrix2 ==");
    	        	for(int n=(4+numbers.get(0)*numbers.get(1));n<numbers.size();n++){
    	        		System.out.println(numbers.get(n));
    	        		matrixArray2.add(numbers.get(n));
    	        	}
    	        }
    	        System.out.println("Method Calling");
    	        MatrixProduct(numbers.get(0),numbers.get(1),numbers.get(2),numbers.get(3),matrixArray1,matrixArray2);
    	        System.out.println("Method Complete");
    	        reader.close();
    	    } catch (Exception e) {
    	        e.printStackTrace();
    	    }
    	}
    }
    
    
    

       public static void MatrixProduct(int m,int n,int p,int q,List<Integer> matrixArray1,List<Integer> matrixArray2)
       {
    	 //writing
           
    	   try {

   			File file = new File("C:\\Users\\Rakesh\\Desktop\\output.txt");

   			// if file doesnt exists, then create it
   			if (!file.exists()) {
   				file.createNewFile();
   			}

   			FileWriter fw = new FileWriter(file.getAbsoluteFile());
   			BufferedWriter bw = new BufferedWriter(fw);
  
    	   int sum = 0, c, d, k;
     
          int first[][] = new int[m][n];
          System.out.println("M="+m+",N="+n+",matrixArray1="+matrixArray1+",matrixArray1="+matrixArray2);
          bw.write("==========================START====================================");
          bw.write("Enter the elements of first matrix");
          
          
          int count=0;
          for ( c = 0 ; c < m ; c++ ){
        	  bw.write("\n");
             for ( d = 0 ; d < n ; d++ ){
          		first[c][d]=matrixArray1.get(count);
          		bw.write(" "+first[c][d]);
          		count++;
             }
          }
          bw.write("\n");
          if ( n != p )
        	  bw.write("Matrices with entered orders can't be multiplied with each other.");
          else
          {
             int second[][] = new int[p][q];
             int multiply[][] = new int[m][q];
     
             bw.write("Enter the elements of second matrix");
             count=0;
             for ( c = 0 ; c < p ; c++ ){
            	 System.out.print("\n");
                for ( d = 0 ; d < q ; d++ ){
              		second[c][d]=matrixArray2.get(count);
              		bw.write(" "+second[c][d]);
              		count++;
                }
             }
             bw.write("\n");
             for ( c = 0 ; c < m ; c++ )
             {
                for ( d = 0 ; d < q ; d++ )
                {   
                   for ( k = 0 ; k < p ; k++ )
                   {
                      sum = sum + first[c][k]*second[k][d];
                   }
     
                   multiply[c][d] = sum;
                   sum = 0;
                }
             }
     
         	bw.write("Product of entered matrices:-");
     
             for ( c = 0 ; c < m ; c++ )
             {
                for ( d = 0 ; d < q ; d++ )
                	bw.write(multiply[c][d]+"\t");
     
                bw.write("\n");
             }
          }
          
          bw.write("===================DONE========================================");
 			bw.close();

 			System.out.println("Done");

 		} catch (IOException e) {
 			e.printStackTrace();
 		}
                 
       }
    
}