package Jyoti;
import java.io.*;
//Reading strings from consolehel
public class ReadLines 
{
	public static void main(String[] args) //throws IOException
	{
	    try
	    {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str;
		System.out.println("Enter lines of text.");
		System.out.println("Enter 'stop' to quit.");
		do
		{
			str = br.readLine(); //read a line from console
			System.out.println(str);
		}while (!str.equals("stop"));
	    }
	    catch (Exception e)
	    {
	    
	    }
	}
}
